﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Models;
using Hte.Repository;
using Hte.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace Hte.Controllers
{
    [Route("api/[controller]")]
    public class NotificationsController : Controller
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly IUserGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICurrentUserAuthorizer _currentUserAuthorizer;

        public NotificationsController(
            INotificationRepository notificationRepository,
            IUserGroupRepository groupRepository,
            IUserRepository userRepository,
            ICurrentUserAuthorizer currentUserAuthorizer
        )
        {
            _notificationRepository = notificationRepository;
            _groupRepository = groupRepository;
            _userRepository = userRepository;
            _currentUserAuthorizer = currentUserAuthorizer;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(
            [FromForm] Notification notification,
            [FromQuery(Name = "user_id")] [MongoObjectId] string userId,
            [FromQuery(Name = "group_id")] [MongoObjectId] string groupId
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            if (notification.Type == NotificationType.User)
            {
                if (string.IsNullOrEmpty(userId))
                {
                    return BadRequest(new {message = "user_id is required for user type notification"});
                }

                if (await _userRepository.FindById(userId, true) == null)
                {
                    return NotFound(new {message = "user not found"});
                }
                
                notification.UsersId = new [] {userId};
            }
            else if (notification.Type == NotificationType.Group)
            {
                if (string.IsNullOrEmpty(groupId))
                {
                    return BadRequest(new {message = "group_id is required for user type notification"});
                }

                var group = await _groupRepository.FindById(groupId);
                if (group == null)
                {
                    return NotFound(new {message = "group not found"});
                }
                
                notification.UsersId = group.UsersId;
            }
            else
            {
                return BadRequest(new {message = "invalid type provided"});
            }

            await _notificationRepository.InsertAsync(notification);
            return CreatedAtRoute("GetNotification", new {id = notification.Id}, notification);
        }

        [HttpGet("{id}", Name = "GetNotification")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> GetNotification(
            [FromQuery] [Required] [MongoObjectId] string id
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var notification = await _notificationRepository.FindByIdAsync(id);
            if (notification == null)
            {
                return NotFound(new {message = "notification not found"});
            }

            var currentUserId = _currentUserAuthorizer.GetUserId(User);
            if (User.IsInRole("User") && !await _notificationRepository.UserIsInNotificationAsync(notification, currentUserId))
            {
                return Forbid();
            }
            
            return Ok(notification);
        }

        [HttpGet("", Name = "GetNotifications")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetList(
            [FromQuery(Name = "seen_status")] NotificationSeenStatus seenStatus,
            [FromQuery] int limit = 10,
            [FromQuery] int offset = 0
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var userId = _currentUserAuthorizer.GetUserId(User);
            return Ok(await _notificationRepository.FindUserNotificationsAsync(userId, seenStatus, limit, offset));
        }

        [HttpPut("{id}/see")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> SeeNotification([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var notification = await _notificationRepository.FindByIdAsync(id);
            if (notification == null)
            {
                return NotFound(new {message = "notification not found"});
            }

            var currentUserId = _currentUserAuthorizer.GetUserId(User);
            if (string.IsNullOrEmpty(notification.UsersId.FirstOrDefault(i => i == currentUserId)))
            {
                return Forbid();
            }

            if (await _notificationRepository.HasUserSeenNotification(notification, currentUserId))
            {
                return BadRequest(new {message = "user has already seen this notification"});
            }

            await _notificationRepository.SeeNotificationAsync(notification, currentUserId);
            return NoContent();
        }
    }
}