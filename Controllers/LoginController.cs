﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Hte.Helpers.Builders;
using Hte.Helpers.RandomHelper;
using Hte.Helpers.Validators;
using Hte.Models;
using Hte.Repository;
using Hte.Services;
using Hte.Services.Messaging;
using Hte.Services.StateSaver;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Controllers
{
    [Route("api/auth")]
    public class LoginController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly UserManager<User> _userManager;
        private readonly IUserBuilder _userBuilder;
        private readonly ISmsSender _smsSender;
        private readonly IRandomGenerator _randomGenerator;
        private readonly TokenProvider _tokenProvider;
        private readonly IUserLoginStateSaver _stateSaver;

        public LoginController(
            IUserRepository userRepository,
            UserManager<User> userManager,
            IUserBuilder userBuilder,
            ISmsSender smsSender,
            IRandomGenerator randomGenerator,
            TokenProvider tokenProvider,
            IUserLoginStateSaver stateSaver
        )
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _userBuilder = userBuilder;
            _smsSender = smsSender;
            _randomGenerator = randomGenerator;
            _tokenProvider = tokenProvider;
            _stateSaver = stateSaver;
        }

        [HttpPost("login/by_code")]
        public async Task<IActionResult> LoginByCode(
            [FromForm] [Required(AllowEmptyStrings = false)] [Mobile]
            string mobile,
            CancellationToken cancellationToken
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = "invalid required mobile" });
            }
            var user = await _userRepository.FindByMobile(mobile, true);
            if (user == null)
            {
                user = _userBuilder.CreateUser().SetMobile(mobile).SetEmail($"{mobile}@localhost.com").SetUserName(mobile).Build();
                await _userManager.CreateAsync(user, mobile);
            }
            
            var loginCode = _randomGenerator.GetRandomNumericalString(5);
            await _smsSender.SendLoginCode(loginCode, mobile);
            
            _stateSaver.RegisterLoginCode(mobile, loginCode);
            
            return Ok(new {message = $"code sent to {mobile} - {loginCode}"});
        }

        [HttpPost("login/by_code/confirm")]
        public async Task<IActionResult> LoginByCodeVerify(LoginConfirmationForm form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            
            if (!_stateSaver.HasUserValidLoginCode(form.Mobile, form.Code))
            {
                return NotFound(new {message = "code expired. try to login again"});
            }

            var user = await _userRepository.FindByMobile(form.Mobile);
            
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }
            
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("role", user.Role)
            };

            var token = _tokenProvider.GenerateToken(claims);
            var exp = _tokenProvider.GetExpDate();

            return Ok(new {id = user.Id, token, exp, role = user.Role, is_new = user.Status == UserStatus.New });
        }
    }
}
