﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Hte.Helpers.RandomHelper;
using Hte.Helpers.Validators;
using Hte.Models;
using Hte.Repository;
using Hte.Services.Mapper;
using Hte.Services.Messaging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Controllers
{
    [Route("/api/[controller]")]
    public class GiftsController : Controller
    {
        private readonly IRepositoryResponseMapper _mapper;
        private readonly IGiftRepository _giftRepository;
        private readonly IDrawChanceRepository _drawChanceRepository;
        private readonly IUserRepository _userRepository;
        private readonly IRandomGenerator _randomGenerator;
        private readonly ISmsSender _smsSender;
        private readonly string _wwwroot;

        public GiftsController(
            IHostingEnvironment hostingEnvironment,
            IRepositoryResponseMapper mapper,
            IGiftRepository giftRepository,
            IDrawChanceRepository drawChanceRepository,
            IUserRepository userRepository,
            RNGCryptoServiceProvider rngCryptoServiceProvider,
            IRandomGenerator randomGenerator,
            ISmsSender smsSender
        )
        {
            _mapper = mapper;
            _giftRepository = giftRepository;
            _userRepository = userRepository;
            _drawChanceRepository = drawChanceRepository;
            _smsSender = smsSender;
            _randomGenerator = randomGenerator;
            _wwwroot = Path.Combine(hostingEnvironment.WebRootPath);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(Gift form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            
            var photos = new List<Photo>();
            foreach (var file in form.Pics)
            {
                if (file.Length <= 0) continue;
                if (file.ContentType != "image/png" && file.ContentType != "image/jpg")
                {
                    return BadRequest(new {message = "image type not supported"});
                }

                var uploadPath = Path.Combine(_wwwroot, "uploads", "gifts", form.Code, file.FileName);
                
                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(uploadPath)));
                
                Path.GetRelativePath(_wwwroot, uploadPath);
                
                using (var fileStream = new FileStream(uploadPath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                photos.Add(new Photo
                {
                    Ext = file.ContentType,
                    Name = file.FileName,
                    Path = Path.GetRelativePath(_wwwroot, uploadPath)
                });
            }

            form.Photos = new List<Photo>().Union(photos).ToList();
            
            form.DateRegistered = DateTime.Now;

            var gift = await _giftRepository.Insert(form);
            return CreatedAtRoute("GetGift", new {id = form.Id}, gift);
        }

        [HttpGet("{id}", Name = "GetGift")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetGift([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var gift = await _giftRepository.FindById(id);
            if (gift == null)
            {
                return NotFound(new {message = "gift not found"});
            }

            return Ok(gift);
        }

        [HttpDelete("{gift_id}/photos/{photo_id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteGiftPhoto(
            [FromRoute(Name = "gift_id")] [Required] [MongoObjectId] string giftId,
            [FromRoute(Name = "photo_id")] [Required] string photoId
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id", details = ModelState.Values});
            }
            
            var gift = await _giftRepository.FindById(giftId);
            if (gift == null)
            {
                return NotFound(new {message = "gift not found"});
            }

            if (int.TryParse(photoId, out var index))
            {
                if (index > gift.Photos.ToArray().Length)
                {
                    return BadRequest(new {message = "out of range index"});
                }

                await _giftRepository.DeletePhotoByIndex(gift, index - 1);
                return NoContent();
            }

            if (!gift.Photos.ToList().Exists(photo => photo.Name == photoId))
            {
                return NotFound(new {message = "image with requested name not found"});
            }
                
            await _giftRepository.DeletePhotoByName(gift, photoId);
            return NoContent();
        }

        [HttpPut("{id}/photos/add")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddGiftPhoto([FromRoute] [Required] [MongoObjectId] string id, MultipleFileUpload multipleFileUpload)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var gift = await _giftRepository.FindById(id);

            if (gift == null)
            {
                return NotFound(new {message = "gift not found"});
            }
            
            var photos = new List<Photo>();
            foreach (var file in multipleFileUpload.Files)
            {
                if (file.Length <= 0) continue;
                if (file.ContentType != "image/png" && file.ContentType != "image/jpeg")
                {
                    return BadRequest(new {message = "image type not supported"});
                }

                var uploadPath = Path.Combine(_wwwroot, "uploads", "gifts", gift.Code, file.FileName);
                
                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(uploadPath)));
                
                Path.GetRelativePath(_wwwroot, uploadPath);
                
                using (var fileStream = new FileStream(uploadPath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                photos.Add(new Photo
                {
                    Ext = file.ContentType,
                    Name = file.FileName,
                    Path = Path.GetRelativePath(_wwwroot, uploadPath)
                });
            }

            await _giftRepository.PushPhotos(gift, photos);
            return NoContent();
        }

        [HttpGet("{id}/candidates", Name = "GetGiftCandidates")]
        public async Task<IActionResult> GetGiftCandidates([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var doesGiftExists = _giftRepository.DoesGiftExists(id, out var gift); 
            if (!doesGiftExists)
            {
                return NotFound(new {message = "gift not found"});
            }

            return Ok(_mapper.MapUserResponses(await _userRepository.FindGiftCandidates(gift)));
        }

        [HttpPost("{id}/draw")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateGiftDraw([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid gift id"});
            }

            var gift = await _giftRepository.FindById(id);
            
            if (gift == null)
            {
                return NotFound(new {message = "gift not found"});
            }

            if (gift.Drawed)
            {
                return BadRequest(new {message = "gift has already been drawn"});
            }

            var drawChances = await _drawChanceRepository.FindGiftChances(gift);
            var enumerable = drawChances as DrawChance[] ?? drawChances.ToArray();
            if (enumerable.ToArray().Length == 0)
            {
                return BadRequest(new {message = "no candidate users"});                
            }
            
            var userIdsMultipliedByChanceQuantity = new List<string>();
            foreach (var drawChance in enumerable)
            {
                for (var i = 0; i < drawChance.Quantity; i++)
                {
                    userIdsMultipliedByChanceQuantity.Add(drawChance.User);
                }
            }

            var temp = userIdsMultipliedByChanceQuantity.OrderBy(e => _randomGenerator.GetCryptoRandomInt());
            
            for (var i = 0; i < 5000; i++)
            {
                temp = userIdsMultipliedByChanceQuantity.OrderBy(e => _randomGenerator.GetCryptoRandomInt());
            }

            var chances = temp.ToArray();

            var winner = await _userRepository.FindById(chances[_randomGenerator.GetRandomInt(chances.Length)]);
            
            Task.WaitAll(
                _smsSender.SendWinningMessage(winner, gift),
                _giftRepository.SetGiftDrawed(gift, winner)
            );

            return CreatedAtRoute("GetUser", _mapper.MapUserResponse(winner));
        }

        [HttpGet("", Name = "GetGifts")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetList(
            [FromQuery(Name = "drawing_status")] GiftDrawingStatus drawingStatus = GiftDrawingStatus.All,
            [FromQuery] int limit = 10,
            [FromQuery] int offset = 0
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request parameteres"});
            }

            return Ok(await _giftRepository.FindAsync(drawingStatus, limit, offset));
        }
    }
}