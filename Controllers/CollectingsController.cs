﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Repository;
using Hte.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Controllers
{
    [Route("api/[controller]")]
    public class CollectingsController : Controller
    {
        private readonly ICollectingRepository _collectingRepository;
        private readonly ICurrentUserAuthorizer _currentUserAuthorizer;

        public CollectingsController(
            ICollectingRepository collectingRepository,
            ICurrentUserAuthorizer currentUserAuthorizer
        )
        {
            _collectingRepository = collectingRepository;
            _currentUserAuthorizer = currentUserAuthorizer;
        }

        [HttpGet("{id}", Name = "GetCollecting")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> GetCollecting([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (User.IsInRole("User") && !_currentUserAuthorizer.IsSelfUserAuthorized(User, id))
            {
                return Unauthorized();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var collecting = await _collectingRepository.FindById(id);
            if (collecting == null)
            {
                return NotFound(new {message = "collecting not found"});
            }

            return Ok(collecting);
        }
    }
}