﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class UserReport
    {
        [BindNever]
        [BsonElement]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [Required]
        [BsonElement]
        [BsonRequired]
        public string Title { get; set; }
        
        [Required]
        [BsonElement]
        [BsonRequired]
        public string Description { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string User { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public DateTime DateCreted { get; set; }

        [BindNever]
        [BsonElement]
        public IEnumerable<string> SeenUserIds { get; set; } = new List<string>();
    }

    public enum UserReportSeenStatus
    {
        Unseen,
        All,
        Seen,
    }
}