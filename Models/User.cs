﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using MongoDB.Bson.Serialization.Attributes;
using Hte.Helpers;
using Hte.Helpers.Validators;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Hte.Models
{
    public class User : Response.UserResponse
    {

        public User()
        {
            DrawChanceIds = new List<string>();
        }
        
        [EmailAddress]
        [BsonElement]
        [BsonDefaultValue("")]
        public string Email { get; set; }
        
        [BsonElement]
        public string Username { get; set; }
        
        [BindNever]
        [BsonElement]
        public string NormalizedUsername { get; set; }

        [BindNever]
        [BsonElement]
        public IList<string> Roles { get; set; } = new List<string>{Constants.Roles.User};
        
        [BindNever]
        [BsonIgnore]
        public string Role
        {
            get => Roles.First();
            set => Roles = new List<string>{value};
        }

        [BindNever]
        [BsonElement]
        [BsonIgnoreIfDefault]
        public IList<Claim> Claims { get; set; } = new List<Claim>();

        [Required]
        [Mobile]
        public string Mobile  { get; set; }

        public string PasswordHash;

        [BindNever]
        public IEnumerable<string> DrawChanceIds { get; set; }

        [BindNever]
        [BsonIgnoreIfDefault]
        public IEnumerable<string> CollectingIds { get; set; }
        
        [BindNever]
        [BsonIgnoreIfDefault]
        public IEnumerable<string> SeenNotification { get; set; }
        
        [BindNever]
        [BsonIgnoreIfDefault]
        public DateTime LastCollecting { get; set; }
        
        [BindNever]
        public DateTime LastUpdateAt { get; set; }
    }

    public enum UserStatus
    {
        Deactive,
        New,
        Active,
    }
}