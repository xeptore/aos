﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Hte.Models
{
    public class MultipleFileUpload
    {
        [Required]
        public IEnumerable<IFormFile> Files { get; set; }
    }
    
    public class FileUpload
    {
        [Required]
        public IFormFile File { get; set; }
    }
}