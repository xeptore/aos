﻿using System;

namespace Hte.Models
{
    public class LoginCodeHolder
    {
        public string Code { get; set; }
        public DateTime RegisteredDate { get; set; }
    }
}