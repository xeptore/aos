﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class TrashCollecting
    {
        [BindNever]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BindNever]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
        
        [Required]
        [FromForm(Name = "date_collected")]
        [BsonElement]
        public DateTime DateCollected { get; set; }
        
        [Required]
        [BsonElement]
        public double Weight { get; set; }
        
        [Required]
        [FromForm(Name = "received_points")]
        [BsonElement]
        public int RecievedPoints { get; set; }
    }
}