﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Models.Form
{
    public class UserEditForm
    {
        [EmailAddress]
        public string Email { get; set; }
        
        public string Username { get; set; }
        
        [FromForm(Name = "old_password")]
        public string OldPassword { get; set; }
        
        [FromForm(Name = "new_password")]
        public string NewPassword { get; set; }
        
        [FromForm(Name = "confirm_password")]
        public string ConfirmPassword { get; set; }
    }
}