﻿namespace Hte.Models
{
    public class SmsOptions
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string BaseSendUrl { get; set; }
        public string SendNumber { get; set; }
    }
}