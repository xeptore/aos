﻿using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class Photo
    {
        [BsonElement]
        public string Path { get; set; }
        
        [BsonElement]
        public string Name { get; set; }
        
        [BsonElement]
        public string Ext { get; set; }
    }
}