﻿using System.ComponentModel.DataAnnotations;
using Hte.Helpers.Validators;

namespace Hte.Models
{
    public class LoginConfirmationForm
    {
        [Required(AllowEmptyStrings = false)]
        [Mobile]
        public string Mobile { get; set; }
        
        [Required]
        public string Code { get; set; }
    }
}