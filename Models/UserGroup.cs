﻿using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Hte.Models
{
    public class UserGroup
    {
        [BindNever]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [Required]
        [BsonRequired]
        [BsonElement]
        public string Name { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public DateTime DateCreated { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public IEnumerable<string> UsersId { get; set; }
    }
}