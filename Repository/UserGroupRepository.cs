﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Hte.Helpers;
using Hte.Models;
using Hte.Models.Context;
using Microsoft.EntityFrameworkCore.Internal;
using MongoDB.Driver;

namespace Hte.Repository
{
    public sealed class UserGroupRepository : IUserGroupRepository
    {
        private readonly IAppContext _context;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserGroupRepository(IAppContext context, IUserRepository userRepository, IMapper mapper)
        {
            _context = context;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserGroup> FindById(string id)
        {
            return await _context.UserGroups.Find(Builders<UserGroup>.Filter.Eq(ug => ug.Id, id)).FirstOrDefaultAsync();
        }

        public async Task<UserGroup> Create(string name)
        {
            var ug = new UserGroup
            {
                Name = name,
                DateCreated = DateTime.Now,
                UsersId = new List<string>()
            };
            await _context.UserGroups.InsertOneAsync(ug);
            return ug;
        }

        public async void AssignUser(UserGroup userGroup, User user)
        {
            await _context.UserGroups.UpdateOneAsync(
                Builders<UserGroup>.Filter.Eq(ug => ug.Id, userGroup.Id),
                Builders<UserGroup>.Update.Push(ug => ug.UsersId, user.Id),
                new UpdateOptions {IsUpsert = true}
            );
        }

        public async Task UnAssignUserAsync(UserGroup userGroup, User user)
        {
            var index = userGroup.UsersId.IndexOf(user.Id);
            var userIds = userGroup.UsersId.ToList();
            userIds.RemoveAt(index);
            await _context.UserGroups.UpdateOneAsync(
                Builders<UserGroup>.Filter.Eq(ug => ug.Id, userGroup.Id),
                Builders<UserGroup>.Update.Set(ug => ug.UsersId, userIds)
            );
        }

        public async Task<IEnumerable<User>> FindGroupUsers(UserGroup userGroup)
        {
            var result = new List<User>();
            foreach (var userId in userGroup.UsersId)
            {
                result.Add(await _userRepository.FindById(userId, true));
            }

            return result;
        }

        public async Task<bool> UserExistsInGroup(UserGroup group, User user)
        {
            var cursor = await _context.UserGroups.FindAsync(
                Builders<UserGroup>.Filter.And(
                    Builders<UserGroup>.Filter.Eq(ug => ug.Id, group.Id),
                    Builders<UserGroup>.Filter.AnyEq(ug => ug.UsersId, user.Id)
                )
            );
            return await cursor.FirstOrDefaultAsync() != null;
        }

        public async Task<IEnumerable<UserGroup>> FindUserGroups(User user)
        {
            return await _context.UserGroups.Find(
                Builders<UserGroup>.Filter.AnyEq(ug => ug.UsersId, user.Id)
            ).ToListAsync();
        }

        public async Task UpdateGroupName(UserGroup userGroup, string newName)
        {
            await _context.UserGroups.UpdateOneAsync(
                Builders<UserGroup>.Filter.Eq(ug => ug.Id, userGroup.Id),
                Builders<UserGroup>.Update.Set(ug => ug.Name, newName)
            );
        }
    }
}