﻿using System.Threading.Tasks;
using Hte.Models;
using Hte.Models.Context;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class CollectingRepository : ICollectingRepository
    {
        private readonly IAppContext _context;

        public CollectingRepository(IAppContext context)
        {
            _context = context;
        }
        
        public async Task Insert(TrashCollecting collecting)
        {
            await _context.Collectings.InsertOneAsync(collecting);
        }

        public async Task<TrashCollecting> FindById(string id)
        {
            return await _context.Collectings.Find(Builders<TrashCollecting>.Filter.Eq(tc => tc.Id, id)).FirstOrDefaultAsync();
        }
    }
}