﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface IGiftRepository
    {
        Task<Gift> Insert(Gift gift);
        
        Task<Gift> FindById(string id);
        
        Task<Gift> DeletePhotoByIndex(Gift gift, int index);
        
        Task<Gift> DeletePhotoByName(Gift gift, string name);
        
        Task<IEnumerable<Gift>> FindUserAvailableGifts(User user, int limit = 10, int offset = 10);
        
        Task<IEnumerable<Gift>> FindAsync(GiftDrawingStatus drawingStatus, int limit = 10, int offset = 0);
        
        Task PushPhotos(Gift gift, IEnumerable<Photo> photos);
        
        Task PushCandidate(Gift gift, DrawChance drawChance);
        
        bool DoesGiftExists(string id, out Gift gift);
        
        Task SetGiftDrawed(Gift gift, User winner);
    }
}