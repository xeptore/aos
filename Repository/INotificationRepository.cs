﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface INotificationRepository
    {
        Task InsertAsync(Notification notification);
        Task<Notification> FindByIdAsync(string id);
        Task<bool> UserIsInNotificationAsync(Notification notification, string userId);

        Task<IEnumerable<Notification>> FindUserNotificationsAsync(
            string userId,
            NotificationSeenStatus seenStatus = NotificationSeenStatus.All,
            int limit = 10,
            int offset = 0
        );

        Task<bool> HasUserSeenNotification(Notification notification, string userId);

        Task SeeNotificationAsync(Notification notification, string userId);
    }
}