﻿using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface ICollectingRepository
    {
        Task Insert(TrashCollecting collecting);
        Task<TrashCollecting> FindById(string id);
    }
}