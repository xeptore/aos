﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface IUserGroupRepository
    {
        Task<UserGroup> FindById(string id);
        Task<UserGroup> Create(string name);
        void AssignUser(UserGroup userGroup, User user);
        Task UnAssignUserAsync(UserGroup userGroup, User user);
        Task<IEnumerable<User>> FindGroupUsers(UserGroup userGroup);
        Task<bool> UserExistsInGroup(UserGroup group, User user);
        Task<IEnumerable<UserGroup>> FindUserGroups(User user);
        Task UpdateGroupName(UserGroup group, string newName);
    }
}