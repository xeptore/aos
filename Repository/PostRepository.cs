﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;
using Hte.Models.Context;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly IAppContext _context;

        public PostRepository(IAppContext context)
        {
            _context = context;
        }
        
        public async Task<Post> FindById(string id)
        {
            return await _context.Posts.Find(Builders<Post>.Filter.Eq(p => p.Id, id)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Post>> Find(int limit = 10, int offset = 0)
        {
            return await _context.Posts.Find(new BsonDocument()).Limit(limit).Skip(offset).ToListAsync();
        }

        public async Task Insert(Post post)
        {
            await _context.Posts.InsertOneAsync(post);
        }

        public async Task Update(string id, Post newPost)
        {
            await _context.Posts.UpdateOneAsync(
                Builders<Post>.Filter.Eq(p => p.Id, id),
                Builders<Post>.Update.Combine(
                    Builders<Post>.Update.Set(p => p.Title, newPost.Title),
                    Builders<Post>.Update.Set(p => p.Description, newPost.Description)
                )
            );
        }

        public async Task<IEnumerable<Post>> FindRecentAsync(int limit = 10, int offset = 0)
        {
            return await _context.Posts
                .Find(new BsonDocument())
                .Sort(Builders<Post>.Sort.Descending(p => p.DateCreated))
                .Limit(limit)
                .Skip(offset)
                .ToListAsync();
        }
    }
}