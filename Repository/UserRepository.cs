﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Helpers.Factories;
using Hte.Models;
using Hte.Models.Context;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoCollection<User> _collection;
        private readonly IMongoCollection<DrawChance> _drawChanceCollection;
        private readonly IUserQueryFilterFactory _userQueryFilterFactory;
        
        public UserRepository(IAppContext ctx, IUserQueryFilterFactory userQueryFilterFactory)
        {
            _collection = ctx.Users;
            _drawChanceCollection = ctx.DrawChances;
            _userQueryFilterFactory = userQueryFilterFactory;
        }

        public async Task<IEnumerable<User>> Find(int limit = 10, int skip = 0, bool useResposeProjection = false)
        {
            var defaultFilters = _userQueryFilterFactory.CreateDefaultFilters();

            var cursor = _collection
                .Find(
                    Builders<User>.Filter.And(defaultFilters)
                )
                .Project<User>(
                    Builders<User>.Projection.Combine(GetDefaultUserResponseProjectionDefinitions())
                );
            
            return await cursor.ToListAsync();
        }

        public async Task<User> FindById(string id, bool useResposeProjection = false)
        {
            var defaultFilters = _userQueryFilterFactory.CreateDefaultFilters();
            defaultFilters.Add(Builders<User>.Filter.Eq(u => u.Id, id));
            var cursor = _collection.Find(Builders<User>.Filter.And(defaultFilters)).Project<User>(Builders<User>.Projection.Combine(GetDefaultUserResponseProjectionDefinitions(useResposeProjection)));

            return await cursor.FirstOrDefaultAsync();
        }

        public async Task<User> FindByMobile(string mobile, bool useResposeProjection = false)
        {
            var defaultFilters = _userQueryFilterFactory.CreateDefaultFilters();
            defaultFilters.Add(Builders<User>.Filter.Eq(u => u.Mobile, mobile));
            var cursor = _collection.Find(Builders<User>.Filter.And(defaultFilters)).Project<User>(Builders<User>.Projection.Combine(GetDefaultUserResponseProjectionDefinitions()));

            return await cursor.FirstOrDefaultAsync();
        }

        public async Task Insert(User user)
        {
            await _collection.InsertOneAsync(user);
        }

        public async Task<bool> HasUserDrawChance(User user, DrawChance drawChance)
        {
            return await _collection
                       .Find(
                           Builders<User>.Filter.And(
                               Builders<User>.Filter.Eq(u => u.Id, user.Id),
                               Builders<User>.Filter.AnyEq(u => u.DrawChanceIds, drawChance.Id)
                           )
                       )
                       .Project(Builders<User>.Projection.Include(u => u.Id))
                       .FirstOrDefaultAsync() != null;
        }

        public async Task AddDrawChance(User user, DrawChance drawChance)
        {
            await _collection.UpdateOneAsync(
                Builders<User>.Filter.Eq(u => u.Id, user.Id),
                Builders<User>.Update.Push(u => u.DrawChanceIds, drawChance.Id)
            );
        }

        public async Task DecreaseUserPoints(User user, int giftPoints)
        {
            await _collection.UpdateOneAsync(
                Builders<User>.Filter.Eq(u => u.Id, user.Id),
                Builders<User>.Update.Inc(u => u.Points, -giftPoints)
            );
        }

        public async Task<IEnumerable<User>> FindGiftCandidates(Gift gift)
        {
            var result = new List<User>();
            foreach (var drawChanceId in gift.DrawChanceIds)
            {
                var user = await _drawChanceCollection.Find(Builders<DrawChance>.Filter.Eq(dc => dc.Id, drawChanceId))
                    .Project(dc => new {Id = dc.User}).FirstOrDefaultAsync();
                result.Add(await FindById(user.Id, true));
            }

            return result;
        }

        public async Task AddUserCollecting(User user, TrashCollecting collecting)
        {
            await _collection
                .UpdateOneAsync(
                    Builders<User>.Filter.Eq(u => u.Id, user.Id),
                    Builders<User>.Update.Combine(
                        Builders<User>.Update.Push(u => u.CollectingIds, collecting.Id),
                        Builders<User>.Update.Set(u => u.LastCollecting, collecting.DateCollected),
                        Builders<User>.Update.Inc(u => u.Points, collecting.RecievedPoints)
                    )
                );
        }

        public async Task UpdateAsync(string id, User newUser)
        {
            var updateDefinitions = new List<UpdateDefinition<User>> {Builders<User>.Update.Set(u => u.LastUpdateAt, DateTime.Now)};
            
            if (!string.IsNullOrEmpty(newUser.Email))
            {
                updateDefinitions.Add(Builders<User>.Update.Set(u => u.Email, newUser.Email));
            }
            
            if (!string.IsNullOrEmpty(newUser.Username))
            {
                updateDefinitions.Add(Builders<User>.Update.Set(u => u.Username, newUser.Username));
            }
            
            await _collection.UpdateOneAsync(
                Builders<User>.Filter.Eq(u => u.Id, id),
                Builders<User>.Update.Combine(
                    updateDefinitions
                )
            );
        }

        public async Task UpdateAvatarAsync(User user, Photo avatar)
        {
            await _collection.UpdateOneAsync(
                Builders<User>.Filter.Eq(u => u.Id, user.Id),
                Builders<User>.Update.Set(u => u.Avatar, avatar)
            );
        }

        private IEnumerable<ProjectionDefinition<User>> GetDefaultUserResponseProjectionDefinitions(
            bool useDefaultProjection = false
        )
        {
            var projectionDefinitions = new List<ProjectionDefinition<User>>();
            if (!useDefaultProjection) return projectionDefinitions;
            var projectionBuilder = new ProjectionDefinitionBuilder<User>();
            projectionDefinitions.Add(projectionBuilder.Include(u => u.Id));
            projectionDefinitions.Add(projectionBuilder.Include(u => u.Status));
            projectionDefinitions.Add(projectionBuilder.Include(u => u.Points));
            return projectionDefinitions;
        }
    }
}
