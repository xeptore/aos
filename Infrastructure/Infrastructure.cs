﻿using System.Security.Cryptography;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Hte.Helpers.Builders;
using Hte.Helpers.Factories;
using Hte.Helpers.RandomHelper;
using Hte.Models;
using Hte.Models.Context;
using Hte.Repository;
using Hte.Services;
using Hte.Services.Mapper;
using Hte.Services.Messaging;
using Hte.Services.StateSaver;
using Hte.Stores;

namespace Hte.Infrastructure
{
    public static class Infrastructure
    {
        public static void AddAppServices(this IServiceCollection services)
        {
            services.AddSingleton<IUserQueryFilterFactory, UserQueryFilterFactory>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IRandomGenerator, RandomGenerator>();
            services.AddSingleton<IUserLoginStateSaver, StateSaver>();
            services.AddSingleton<IUserFactory, UserFactory>();
            services.AddSingleton<ISmsSender, SmsSender>();
            services.AddSingleton<IAppContext, AppContext>();
            
            services.AddScoped<IRepositoryResponseMapper, RepositoryResponseMapper>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<ICurrentUserAuthorizer, CurrentUserAuthorizer>();
            services.AddScoped<IDrawChanceRepository, DrawChanceRepository>();
            services.AddScoped<ICollectingRepository, CollectingRepository>();
            services.AddScoped<IUserGroupRepository, UserGroupRepository>();
            services.AddScoped<IReportRepository, ReportRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IGiftRepository, GiftRepository>();
            
            services.AddTransient<IUserStore<User>, MongoUserStore>();
            services.AddTransient<IUserBuilder, UserBuilder>();
            services.AddTransient<TokenProvider>();
            services.AddTransient<RNGCryptoServiceProvider>();
        }
    }
}