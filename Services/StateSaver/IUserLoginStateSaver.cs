﻿namespace Hte.Services.StateSaver
{
    public interface IUserLoginStateSaver : IUserStateSaver
    {
        bool HasUserValidLoginCode(string mobile, string codeToValidate);
        void RegisterLoginCode(string mobile, string code);
    }
}