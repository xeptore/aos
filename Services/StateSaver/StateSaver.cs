﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Timers;
using Hte.Models;

namespace Hte.Services.StateSaver
{
    public class StateSaver : IUserLoginStateSaver
    {
        private readonly AuthOptions _authOptions;
        private readonly IDictionary<string, string> _loginCodes;
        private readonly Dictionary<string, Timer> _timers;

        public StateSaver(IOptions<Settings> options)
        {
            _authOptions = options.Value.AuthOptions;
            _timers = new Dictionary<string, Timer>();
            _loginCodes = new Dictionary<string, string>();
        }
        
        public bool HasUserValidLoginCode(string mobile, string codeToValidate)
        {
            return _loginCodes.TryGetValue(mobile, out var loginCode) && loginCode == codeToValidate;
        }

        public void RegisterLoginCode(string mobile, string code)
        {
            if (_loginCodes.ContainsKey(mobile))
            {
                return;
            }

            _loginCodes.Add(mobile, code);

            SetAndStartTimer(mobile);
        }

        private void SetAndStartTimer(string mobile)
        {
            var timer = new Timer
            {
                AutoReset = false,
                Enabled = true,
                Interval = _authOptions.LoginTimerInterval
            };
            
            timer.Elapsed += delegate {
                timer.Stop();
                timer.Close();
                timer.Dispose();
                _loginCodes.Remove(mobile);
                _timers.Remove(mobile);
            };
            
            _timers.Add(mobile, timer);
            
            timer.Start();
        }
    }
}