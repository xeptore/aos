﻿using System.Collections.Generic;
using Hte.Models;
using Hte.Models.Form;
using Hte.Models.Response;

namespace Hte.Services.Mapper
{
    public interface IRepositoryResponseMapper
    {
        UserResponse MapUserResponse(User user);
        
        User MapEditForm(UserEditForm editForm);

        IEnumerable<UserResponse> MapUserResponses(IEnumerable<User> user);
    }
}