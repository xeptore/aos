﻿using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using TokenOptions = Hte.Models.TokenOptions;
using Hte.Helpers;
using Hte.Infrastructure;
using Hte.Models;
using Hte.Services;

namespace Hte
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new SnakeCaseNamingStrategy(true, true)
                    };
                });
            
            services.AddAutoMapper();

            // Application options configuration
            services.Configure<Settings>(options =>
                {
                    options.ConnectionString = _configuration["MongoDB:ConnectionString"];
                    options.Database = _configuration["MongoDB:Database"];
                    options.TokenOptions = new TokenOptions
                    {
                        Audience = _configuration["TokenOptions:Audience"],
                        ExpiresTimeInMinutes = _configuration["TokenOptions:ExpiresTimeAsMinutes"],
                        Issuer = _configuration["TokenOptions:Issuer"],
                        Key = _configuration["TokenOptions:Key"]
                    };
                    options.AuthOptions = new AuthOptions
                    {
                        LoginTimerInterval = _configuration.GetValue<int>("Auth:Login:TimerInterval")
                    };
                    options.SmsOptions = new SmsOptions
                    {
                        BaseSendUrl = _configuration["Sms:BaseUrl:Send"],
                        SendNumber = _configuration["Sms:SendNumber"],
                        UserName = _configuration["Sms:Username"],
                        Password = _configuration["Sms:Password"]
                    };
                }
            );

            services.AddIdentity<User, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 0;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                })
                .AddDefaultTokenProviders();
            
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = _configuration["TokenOptions:Issuer"],
                        ValidAudience = _configuration["TokenOptions:Audience"],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["TokenOptions:Key"]))
                    };
                });

            services.AddHttpClient();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Constants.Policies.MustBeAdmin,
                    policy => { policy.RequireRole(Constants.Roles.Admin); });
                options.AddPolicy(Constants.Policies.MustBeUser,
                    policy => { policy.RequireRole(Constants.Roles.User); });
            });
            
            // Application-specific services used for Dependency Injection
            services.AddAppServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

//            app.UseHttpsRedirection();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = new FileExtensionContentTypeProvider(
                    new Dictionary<string, string>
                    {
                        {".yml", "text/x-yaml"}
                    }
                )
            });
        
            app.UseAuthentication();
            
            app.UseMvc();
        }
    }
}