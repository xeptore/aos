﻿using Hte.Models;

namespace Hte.Helpers.Factories
{
    public class UserFactory : IUserFactory
    {
        public User CreateUser()
        {
            return new User();
        }
    }
}