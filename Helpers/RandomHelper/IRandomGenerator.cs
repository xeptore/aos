﻿namespace Hte.Helpers.RandomHelper
{
    public interface IRandomGenerator
    {
        int GetRandomInt(int min, int max);
        int GetRandomInt(int max);
        string GetRandomNumericalString(int length);
        string GetRandomString(int length);
        string GetRandomString(int length, RandomStringCaps caps);
        int GetCryptoRandomInt();
    }
}