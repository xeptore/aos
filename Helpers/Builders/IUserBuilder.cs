﻿using Hte.Models;

namespace Hte.Helpers.Builders
{
    public interface IUserBuilder
    {
        IUserBuilder CreateUser();
        IUserBuilder SetUserName(string username);
        IUserBuilder SetPassword(string password);
        IUserBuilder SetMobile(string mobile);
        IUserBuilder SetFirstName(string firstName);
        IUserBuilder SetLastName(string lastname);
        IUserBuilder SetEmail(string email);
        IUserBuilder SetRole(string role);
        User Build();
    }
}